# Kickshaw - A Menu Editor for Openbox

Git mirror of [Kickshaw](https://savannah.nongnu.org/projects/obladi) used for Arch packaging

![alt text](https://cdn.scrot.moe/images/2017/08/04/kickshaw.png)

## COMPILING AND INSTALLING THE PROGRAM

A makefile is included in the source directory. "make" and "make install"
are sufficient to compile and install this program; there is no configure 
script file which has to be started first. Having a GTK3 devel package 
installed should bring with it everything that is needed for compilation.


For Arch based distros, a package is available in the [AUR](https://aur.archlinux.org/packages/kickshaw/).

## DEPENDENCIES

Kickshaw is only dependent on GTK3. The code makes use of GNU extensions, 
so a compiler is needed that supports them. Since the standard compilers 
on current systems provide such support this shouldn't cause any extra 
preparations. The standard used for compilation is by default gnu99.

The program is not dependent on Openbox; it can be used inside all window 
managers/desktop environments that support GTK applications to create and 
edit menu files.

## REQUIREMENTS REGARDING MENU FILES

The program uses GLibs XML subset parser to read menu files, so it will be 
able to read a menu file regardless its formatting; it is necessary though 
that the file is encoded in UTF-8. In the case of an error the program 
gives a detailed error message, if possible. In some cases, for example if 
options have a wrong value or an image file that can't be loaded, 
the program will offer the option to correct such errors to avoid a 
premature termination.

## FEATURES

- Support for icons. Changes done outside the program to the image files, 
  for example a replacement of the image file with a new one under the 
  same name, are taken over within a second, and the program will show the 
  new image.
- (Unlimited) Undo/Redo
- (Multirow) Drag and drop
- Search functionality with optional case sensitivity, "word only" search 
  and processing of regular expressions (incl. validity check)
- Find and replace with the same options as for the search functionality 
  and prevention of invalid replacements (creating double menu IDs, 
  "enabled" options other than "yes" and "no")
- Double menu IDs are prevented, either by entering them manually or by a 
  find and replace action.
- Auto-backup of overwritten menu (can be deactivated)
- Auto-reconfiguration of the menu after saving
- Context menu
- Editable cells (dependent on the type of the cell)
- Fast and compact, avoidance of overhead (programmed natively in C, 
  only one additional settings file created by the program itself, 
  no use of Glade)
- Dynamic, context sensitive interface
- The tree view can be customised (show/hide columns, grid etc.)
- In the "About" dialog, users are informed about the availability of a 
  newer version than the one currently installed (requires active internet 
  connection).
- The program informs the user if there are missing menu/item labels, 
  invalid icon paths and menus defined outside the root menu that are not 
  included inside the latter. By request the program creates labels for 
  these invisible menus/items, integrates the orphaned menus into the root 
  menu and opens a file chooser dialog so that invalid icon paths can be 
  corrected.
- Deprecated "execute" options are converted to "command" options
- The options of an "Execute" action and "startupnotify" option block can 
  be auto-sorted to obtain a constant menu structure

## SPECIAL NOTE ABOUT DRAG AND DROP

GTK does not support multirow drag and drop, that's why only one row is 
shown as dragged if mulitple rows have been selected for drag and drop. 
There is a workaround for this, but as good as it works for list views 
it doesn't for tree views, so it wasn't implemented here.

It is actually best to click on the type column first to start a 
drag and drop, since the type column does not contain editable cells and 
thus an unwanted editing will not get in the way.

## License

Copyright (c) 2010–2019        Marcus Schätzle

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Kickshaw. If not, see http://www.gnu.org/licenses/.
