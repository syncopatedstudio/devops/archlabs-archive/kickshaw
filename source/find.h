/*
   Kickshaw - A Menu Editor for Openbox

   Copyright (c) 2010–2019        Marcus Schätzle

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along 
   with Kickshaw. If not, see http://www.gnu.org/licenses/.
*/

#ifndef __find_h
#define __find_h

extern ks_data ks;

extern gboolean avoid_overlapping (G_GNUC_UNUSED gpointer user_data);
extern void row_selected (void);
extern void show_errmsg (gchar *errmsg_raw_txt);
extern void wrong_or_missing (GtkWidget *widget, GtkCssProvider *css_provider);

#endif
