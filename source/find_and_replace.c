/*
   Kickshaw - A Menu Editor for Openbox

   Copyright (c) 2010–2019        Marcus Schätzle

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along 
   with Kickshaw. If not, see http://www.gnu.org/licenses/.
*/

#include <gtk/gtk.h>

#include "definitions_and_enumerations.h"
#include "find_and_replace.h"

typedef struct {
    GSList *menu_IDs_before_and_after;
    guint replacements_done_cnt;
    gboolean no_invalid_replacements;
} FRData;

static void fr_buttons_management (gchar *column_check_button_clicked);
static void entry_field_content_changed (G_GNUC_UNUSED gpointer user_data);
static gchar *compute_hypothetical_replacement (gchar *value);
static gboolean validity_check (              GtkTreeModel *model, 
                                G_GNUC_UNUSED GtkTreePath  *path, 
                                              GtkTreeIter  *iter, 
                                              gpointer      fr_data_pnt);
static gboolean find_and_replace_iteration (G_GNUC_UNUSED GtkTreeModel *model,  
                                            G_GNUC_UNUSED GtkTreePath  *path, 
                                                          GtkTreeIter  *iter, 
                                                          gpointer      fr_data_pnt);
static void find_and_replace_intialisation (G_GNUC_UNUSED gpointer user_data);
void show_find_and_replace_dialog (void);

/* 

    (De)activates all other column check buttons if "All columns" is (un)selected. 

*/

void fr_buttons_management (gchar *column_check_button_clicked)
{
    gboolean all_columns_check_button_clicked = STREQ (column_check_button_clicked, "all");
    gboolean marking_active;
    marking_active = gtk_style_context_has_class (gtk_widget_get_style_context (ks.fr_in_all_columns), 
                                                  "mandatory_missing");

    if (marking_active || all_columns_check_button_clicked) {
        gboolean find_in_all_activated = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (ks.fr_in_all_columns));

        guint8 columns_cnt;

        for (columns_cnt = 0; columns_cnt < COL_ELEMENT_VISIBILITY; columns_cnt++) {
            if (columns_cnt == COL_TYPE) {
                continue;
            }
            if (all_columns_check_button_clicked) {
                g_signal_handler_block (ks.fr_in_columns[columns_cnt], ks.handler_id_fr_in_columns[columns_cnt]);
                gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (ks.fr_in_columns[columns_cnt]), find_in_all_activated);
                g_signal_handler_unblock (ks.fr_in_columns[columns_cnt], ks.handler_id_fr_in_columns[columns_cnt]);
                gtk_widget_set_sensitive (ks.fr_in_columns[columns_cnt], !find_in_all_activated);
            }
            if (marking_active) {
                gtk_style_context_remove_class (gtk_widget_get_style_context (ks.fr_in_columns[columns_cnt]), 
                                                "mandatory_missing");
            }
        }
        if (marking_active) {
            gtk_style_context_remove_class (gtk_widget_get_style_context (ks.fr_in_all_columns), "mandatory_missing");
        }
    }
}

/*

    Activates the "Replace" button if something is entered in one of the entry fields and 
    deactivates it if both entry fields no longer contain any text.

*/

void entry_field_content_changed (G_GNUC_UNUSED gpointer user_data)
{
    gtk_dialog_set_response_sensitive (GTK_DIALOG (ks.fr_dialog), 1, 
                                       *(gtk_entry_get_text (GTK_ENTRY (ks.fr_entry_fields[REPLACE_ENTRY]))) || 
                                       *(gtk_entry_get_text (GTK_ENTRY (ks.fr_entry_fields[WITH_ENTRY]))));
}

/*

    If a find and replace is possible for a certain value, the new value is returned. If it is not possible NULL is returned. 

*/

static gchar *compute_hypothetical_replacement (gchar *value)
{
    const gchar *replace_entry_str = gtk_entry_get_text (GTK_ENTRY (ks.fr_entry_fields[REPLACE_ENTRY]));
    const gchar *with_entry_str = gtk_entry_get_text (GTK_ENTRY (ks.fr_entry_fields[WITH_ENTRY]));

    gchar *replace_entry_str_escaped = (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (ks.fr_special_options[REGULAR_EXPRESSION]))) ? 
                                       NULL : g_regex_escape_string (replace_entry_str, -1);
    gboolean whole_word = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (ks.fr_special_options[WHOLE_WORD]));
    gchar *final_replace_str = g_strconcat ((whole_word) ? "\\b(" : "", 
                                            (replace_entry_str_escaped) ? replace_entry_str_escaped : replace_entry_str, 
                                            (whole_word) ? ")\\b" : "", 
                                            NULL);
    gchar *new_value = NULL; // Default, no match found.

    if (g_regex_match_simple (final_replace_str, value, 
                              (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (ks.fr_special_options[MATCH_CASE]))) ? 
                              0 : G_REGEX_CASELESS, 
                              G_REGEX_MATCH_NOTEMPTY)) {
        GRegex *regex = g_regex_new (final_replace_str, 
                                     (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (ks.fr_special_options[MATCH_CASE]))) ? 
                                     0 : G_REGEX_CASELESS, 0, NULL);

        new_value = g_regex_replace (regex, value, -1, 0, with_entry_str, 0, NULL);

        // Cleanup
        g_regex_unref (regex);
    }

    // Cleanup
    g_free (replace_entry_str_escaped);
    g_free (final_replace_str);

    return (new_value);
}

/* 

    Makes sure that there will be no...
    1. ...values for the "enabled" option other than "yes" and "no"...
    2. ...double menu IDs...
    3. ...empty (pipe) menu labels...
    ...after a find and replace.

*/

static gboolean validity_check (              GtkTreeModel *model,  
                                G_GNUC_UNUSED GtkTreePath  *path, 
                                              GtkTreeIter  *iter,
                                              gpointer      fr_data_data_pnt)
{
    FRData *fr_data = (FRData *) fr_data_data_pnt;

    const gchar *with_entry_str = gtk_entry_get_text (GTK_ENTRY (ks.fr_entry_fields[WITH_ENTRY]));
          gboolean *no_invalid_replacements = &(fr_data->no_invalid_replacements);

    gchar *menu_element_txt, *type_txt;

    gtk_tree_model_get (model, iter, 
                        TS_MENU_ELEMENT, &menu_element_txt, 
                        TS_TYPE, &type_txt,
                        -1);

    // Make sure that there will be no values for the "enabled" option other than "yes" and "no" after a find and replace.

    if (STREQ (type_txt, "option") && STREQ (menu_element_txt, "enabled") &&
        gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (ks.fr_in_columns[COL_VALUE]))) {
        gchar *value, *new_value = NULL; // setting to NULL avoids compilter warning.

        gtk_tree_model_get (model, iter, TS_VALUE, &value, -1);
        if ((new_value = compute_hypothetical_replacement (value)) && 
            !streq_any (new_value, "yes", "no", NULL)) {
            gchar *errmsg_txt = g_strdup_printf ("An <b>\"enabled\"</b> option can't have a value other than "
                                                 "<b>\"yes\"</b> or <b>\"no\"</b> and thus a replacement of "
                                                 "\"%s\" with \"%s\" won't be done.\n\n"
                                                 "<b><span foreground='darkred'>Find and replace action not executed.</span></b>", 
                                                 value, with_entry_str);
            show_errmsg (errmsg_txt);

            *no_invalid_replacements = FALSE;

            // Cleanup
            g_free (errmsg_txt);
        }

        // Cleanup
        g_free (value);
        g_free (new_value);

        if (!(*no_invalid_replacements)) {
            g_free (menu_element_txt);
            g_free (type_txt);

            return TRUE; // Stop iterating through the tree store.
        }
    }

    // Make sure that there will be no double menu IDs after a find and replace.

    if (streq_any (type_txt, "menu", "pipe menu", NULL) && 
        gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (ks.fr_in_columns[COL_MENU_ID]))) {
        gchar *menu_id_txt, *new_menu_id_txt;

        GSList *menu_ids_loop;

        gtk_tree_model_get (model, iter, TS_MENU_ID, &menu_id_txt, -1);
        if ((new_menu_id_txt = compute_hypothetical_replacement (menu_id_txt))) {
            gchar *menu_IDs_before_and_after_txt;

            for (menu_ids_loop = ks.menu_ids; menu_ids_loop; menu_ids_loop = menu_ids_loop->next) {
                if (STREQ (menu_ids_loop->data, new_menu_id_txt) && !STREQ (new_menu_id_txt, menu_id_txt)) {
                    gchar *errmsg_txt = g_strdup_printf ("The menu ID <b>\"%s\"</b> won't be replaced with <b>\"%s\"</b>, "
                                                         "because this would create a double menu ID.\n\n"
                                                         "<b><span foreground='darkred'>Find and replace action not executed.</span></b>", 
                                                         menu_id_txt, new_menu_id_txt);
                    show_errmsg (errmsg_txt);

                    *no_invalid_replacements = FALSE;

                    // Cleanup
                    g_free (errmsg_txt);
                }
            }

            /* 
               Double menu IDs can also occur if for example for both menu IDs "menu1" and "menu2" the trailing numbers are removed.
               All hypothetical new menu IDs are stored inside a list, and then sorted, because with that it is enough to compare a 
               menu ID with the following one inside the list. If they are identical, there would be a double menu ID after the 
               execution of the "find and replace" action.
            */
            menu_IDs_before_and_after_txt = g_strdup_printf ("%s%c%s", new_menu_id_txt, 31, menu_id_txt); // 31 = unit separator    
            fr_data->menu_IDs_before_and_after = g_slist_prepend (fr_data->menu_IDs_before_and_after, menu_IDs_before_and_after_txt);
            fr_data->menu_IDs_before_and_after = g_slist_sort (fr_data->menu_IDs_before_and_after, (GCompareFunc) strcmp);

            if (g_slist_length (fr_data->menu_IDs_before_and_after) > 1) {
                gchar **menu_IDs_before_and_after[2];
 
                GSList *menu_ids_loop;

                for (menu_ids_loop = fr_data->menu_IDs_before_and_after; menu_ids_loop; menu_ids_loop = menu_ids_loop->next) {
                    if (menu_ids_loop->next) {
                        menu_IDs_before_and_after[0] = g_strsplit (menu_ids_loop->data, "\x1F", -1); // x1F (dec. 31) = unit separator
                        menu_IDs_before_and_after[1] = g_strsplit (menu_ids_loop->next->data, "\x1F", -1); // x1F (dec. 31) = unit separator
                        if (STREQ (menu_IDs_before_and_after[0][0], menu_IDs_before_and_after[1][0])) {
                            gchar *errmsg_txt = g_strdup_printf ("A find and replace with these values would replace both menu IDs "
                                                                 "<b>\"%s\"</b> and <b>\"%s\"</b> with <b>\"%s\"</b>, thus creating a "
                                                                 "double menu ID.\n\n"
                                                                 "<b><span foreground='darkred'>Find and replace action "
                                                                 "not executed.</span></b>", 
                                                                 menu_IDs_before_and_after[0][1], 
                                                                 menu_IDs_before_and_after[1][1],
                                                                 menu_IDs_before_and_after[0][0]);
                            show_errmsg (errmsg_txt);

                            *no_invalid_replacements = FALSE;

                            // Cleanup
                            g_free (errmsg_txt);
                        }

                        // Cleanup
                        g_strfreev (menu_IDs_before_and_after[0]);
                        g_strfreev (menu_IDs_before_and_after[1]);
                    }
                }
            }
        }

        // Cleanup
        g_free (menu_id_txt);
        g_free (new_menu_id_txt);

        if (!(*no_invalid_replacements)) {
            g_free (menu_element_txt);
            g_free (type_txt);

            return TRUE; // Stop iterating through the tree store.
        }
    }

    // Make sure that there will be no empty (pipe) menu labels after a find and replace.

    if (!(*with_entry_str) && streq_any (type_txt, "menu", "pipe menu", NULL) && 
        gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (ks.fr_in_columns[COL_MENU_ELEMENT]))) {
        gchar *new_menu_element_txt = compute_hypothetical_replacement (menu_element_txt);

        if (STREQ (new_menu_element_txt, "")) {
            gchar *menu_id_txt;

            gtk_tree_model_get (model, iter, TS_MENU_ID, &menu_id_txt, -1);

            gchar *errmsg_txt = g_strdup_printf ("The %s menu <b>\"%s\"</b> with the menu ID <b>\"%s\"</b> won't receive an "
                                                 "empty label, because this would render the menu invisible.\n\n"
                                                 "<b><span foreground='darkred'>Find and replace action not executed.</span></b>", 
                                                 (STREQ (type_txt, "menu")) ? "" : "pipe", menu_element_txt, menu_id_txt);
            show_errmsg (errmsg_txt);

            *no_invalid_replacements = FALSE;

            // Cleanup
            g_free (menu_id_txt);
            g_free (errmsg_txt);
        }

        // Cleanup
        g_free (new_menu_element_txt);
    }

    // Cleanup
    g_free (menu_element_txt);
    g_free (type_txt);

    return (!(*no_invalid_replacements)); // return value == TRUE -> Stop iterating through the tree store.
}

/*

    Execute a find and replace. "Type" and "Element visibilty" are excluded, since their values have to be unmodifiable.
    This, the uniqueness of each menu ID and the only two possible values for the "enabled" option ("yes" and "no") have already 
    been taken care of.

*/

static gboolean find_and_replace_iteration (              GtkTreeModel *model,  
                                            G_GNUC_UNUSED GtkTreePath  *path, 
                                                          GtkTreeIter  *iter,
                                                          gpointer      fr_data_pnt)
{
    FRData *fr_data = (FRData *) fr_data_pnt;

          gchar *current_column_txt;
          gchar *new_column_txt;
    const gchar *replace_entry_str = gtk_entry_get_text (GTK_ENTRY (ks.fr_entry_fields[REPLACE_ENTRY]));
    const gchar *with_entry_str = gtk_entry_get_text (GTK_ENTRY (ks.fr_entry_fields[WITH_ENTRY]));
          gchar *replace_entry_str_escaped;
          gboolean whole_word = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (ks.fr_special_options[WHOLE_WORD]));
          gchar *final_replace_str;
          GRegex *regex;
          GMatchInfo *match_info;
          guint *replacements_done_cnt = &(fr_data->replacements_done_cnt);
          gchar *type_txt;

    guint8 columns_cnt;

    for (columns_cnt = 0; columns_cnt < NUMBER_OF_COLUMNS - 1; columns_cnt++) {
        gtk_tree_model_get (model, iter, TS_TYPE, &type_txt, -1);

        if (columns_cnt == COL_TYPE || !gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (ks.fr_in_columns[columns_cnt])) || 
            (columns_cnt == COL_MENU_ELEMENT && streq_any (type_txt, "action", "option block", "option", NULL))) {
            // Cleanup
            g_free (type_txt);

            continue;
        }

        gtk_tree_model_get (model, iter, columns_cnt + TREEVIEW_COLUMN_OFFSET, &current_column_txt, -1);

        if (!(*replace_entry_str)) {
           // The program allows to find empty fields and fill them with a value, provided they can also be entered manually.
           if (!NOT_NULL_AND_NOT_EMPTY (current_column_txt) && 
               // The invalid types "action", "option block" and "option" have already been taken care above.
               (columns_cnt == COL_MENU_ELEMENT || 
                (columns_cnt == COL_VALUE && STREQ (type_txt, "option")) || 
                (columns_cnt == COL_MENU_ID && STREQ (type_txt, "menu")) || 
                (columns_cnt == COL_EXECUTE && STREQ (type_txt, "menu") && !gtk_tree_model_iter_has_child (model, iter)))) {
               gtk_tree_store_set (ks.treestore, iter, columns_cnt + TREEVIEW_COLUMN_OFFSET, with_entry_str, -1);

               /* It's simpler to overwrite the visibility status regardless of the previous status than to query the status first.
                  Only the status of the types "menu", "pipe menu", "item" and "separator" is overwritten, since the other types 
                  were already skipped above. */
               if (columns_cnt == COL_MENU_ELEMENT) {
                   gtk_tree_store_set (ks.treestore, iter, TS_ELEMENT_VISIBILITY, "visible", -1);
               }

               if (columns_cnt == COL_MENU_ID) {
                   remove_menu_id ("");
                   ks.menu_ids = g_slist_prepend (ks.menu_ids, g_strdup (with_entry_str));    
               }

               // By adding a text to the "Execute" field, a menu without children has been turned into a pipe menu.
               if (columns_cnt == COL_EXECUTE && STREQ (type_txt, "menu") && !gtk_tree_model_iter_has_child (model, iter)) {
                   gtk_tree_store_set (ks.treestore, iter, TS_TYPE, "pipe menu", -1);
               }

               (*replacements_done_cnt)++;
           }

           // Cleanup
           g_free (type_txt);
           g_free (current_column_txt);

           continue;
        }

        if (!current_column_txt) {
            // Cleanup
            g_free (type_txt);

            continue;
        };

        replace_entry_str_escaped = (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (ks.fr_special_options[REGULAR_EXPRESSION]))) ? 
                                     NULL : g_regex_escape_string (replace_entry_str, -1);
        final_replace_str = g_strconcat ((whole_word) ? "\\b(" : "", 
                                         (replace_entry_str_escaped) ? replace_entry_str_escaped : replace_entry_str, 
                                         (whole_word) ? ")\\b" : "", 
                                         NULL);

        regex = g_regex_new (final_replace_str, 
                             (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (ks.fr_special_options[MATCH_CASE]))) ? 
                             0 : G_REGEX_CASELESS, 0, NULL);

        // Cleanup
        g_free (replace_entry_str_escaped);
        g_free (final_replace_str);

        if (!g_regex_match (regex, current_column_txt, 0, &match_info)) {
            // Cleanup
            g_free (type_txt);
            g_free (current_column_txt);
            g_regex_unref (regex);
            g_match_info_free (match_info);

            continue;
        }

        new_column_txt = g_regex_replace (regex, current_column_txt, -1, 0, with_entry_str, 0, NULL);

        // The program won't store empty separator labels; a separator should be either with or without text.
        gtk_tree_store_set (ks.treestore, iter, columns_cnt + TREEVIEW_COLUMN_OFFSET, 
                            !(!(*new_column_txt) && STREQ (type_txt, "separator")) ? new_column_txt : NULL, -1);

        if (columns_cnt == COL_MENU_ID) {
            remove_menu_id (current_column_txt);
            ks.menu_ids = g_slist_prepend (ks.menu_ids, g_strdup (new_column_txt));
        }

        while (g_match_info_matches (match_info)) {
            (*replacements_done_cnt)++;
            g_match_info_next (match_info, NULL);
        }

        // Cleanup
        g_regex_unref (regex);
        g_match_info_free (match_info);
        g_free (type_txt);
        g_free (current_column_txt);
        g_free (new_column_txt);
    }

    return FALSE;
}

/*

    Does a number of checks, then starts the actual find and replace action.

*/

static void find_and_replace_intialisation (G_GNUC_UNUSED gpointer user_data)
{       
    FRData fr_data = {
        .menu_IDs_before_and_after = NULL,
        .replacements_done_cnt = 0, // Default
        .no_invalid_replacements = TRUE // Default
    };

    guint8 columns_cnt;

    // If regular expressions are activated, check if the regular expression is valid.
    if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (ks.fr_special_options[REGULAR_EXPRESSION]))) {
        GError *error = NULL;
        GRegex *regex = g_regex_new (gtk_entry_get_text (GTK_ENTRY (ks.fr_entry_fields[REPLACE_ENTRY])), 0, 0, &error);

        if (error) {
            show_errmsg (error->message);

            // Cleanup
            g_error_free (error);            

            return;
        }

        // Cleanup
        g_regex_unref (regex);
    }

    gboolean no_find_in_columns_buttons_clicked = TRUE; // Default

    for (columns_cnt = 0; columns_cnt < COL_ELEMENT_VISIBILITY; columns_cnt++) {
        if (columns_cnt == COL_TYPE) {
            continue;
        }
        if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (ks.fr_in_columns[columns_cnt]))) {
            no_find_in_columns_buttons_clicked = FALSE;
            break;
        }
    }

    if (no_find_in_columns_buttons_clicked) { // No column chosen = not a valid find & replace
        for (columns_cnt = 0; columns_cnt < COL_ELEMENT_VISIBILITY; columns_cnt++) {
            if (columns_cnt == COL_TYPE) {
                continue;
            }
            wrong_or_missing (ks.fr_in_columns[columns_cnt], ks.fr_in_columns_css_providers[columns_cnt]);
        }
        wrong_or_missing (ks.fr_in_all_columns, ks.fr_in_all_columns_css_provider);

        return;
    }

    gtk_tree_model_foreach (ks.model, (GtkTreeModelForeachFunc) validity_check, &fr_data);

    if (fr_data.no_invalid_replacements) {
        gchar *replacements_done_label_txt;
        gchar *small_numbers_spelled_out[] = { "once", "twice", "three", "four", "five", "six", "seven", "eight", "nine" };
        
        fr_data.replacements_done_cnt = 0; // Default

        gtk_tree_model_foreach (ks.model, (GtkTreeModelForeachFunc) find_and_replace_iteration, &fr_data);

        replacements_done_label_txt = (fr_data.replacements_done_cnt) ? 
                                      ((fr_data.replacements_done_cnt < 10) ? 
                                       g_strconcat ("Search key replaced ", 
                                                    small_numbers_spelled_out[(fr_data.replacements_done_cnt) - 1], 
                                                    (fr_data.replacements_done_cnt < 3) ? "." : " times.", NULL) :
                                       g_strdup_printf ("Search key replaced %i times.", fr_data.replacements_done_cnt)) :
                                       "Search key not found.";
        gtk_label_set_text (GTK_LABEL (ks.fr_replacements_done_label), replacements_done_label_txt);

        if (fr_data.replacements_done_cnt) {
            push_new_item_on_undo_stack ();
            activate_change_done ();
            row_selected ();

            // Cleanup
            g_free (replacements_done_label_txt);
        }
        // No dynamic memory is allocated for the static phrase "Search key not found.", so there is no memory freeing for it here.
    }

    // Cleanup. Setting to NULL is unnecessary, since this is part of a local struct and always initialised to NULL in the beginning.
    g_slist_free_full (fr_data.menu_IDs_before_and_after, (GDestroyNotify) g_free);
}

/* 

    Shows the find and replace dialog window.

*/

void show_find_and_replace_dialog (void)
{
    ks.fr_dialog = gtk_dialog_new_with_buttons ("Find & Replace", GTK_WINDOW (ks.window), GTK_DIALOG_DESTROY_WITH_PARENT, 
                                                "_Replace", 1, "_Close", 2, NULL);
    ks.fr_entry_fields[REPLACE_ENTRY] = gtk_entry_new ();
    ks.fr_entry_fields[WITH_ENTRY] = gtk_entry_new ();
    ks.fr_replacements_done_label = gtk_label_new (NULL);
#if GTK_CHECK_VERSION(3,16,0)
    gtk_label_set_xalign (GTK_LABEL (ks.fr_replacements_done_label), 0.0);
#else
/* 
    This would result in a warning when compiling with GTK 3.14, because gtk_misc_set_alignment() has been deprecated with 3.14, 
    but gtk_label_set_xalign is only available since 3.16. With gtk_widget_set_halign() it is not possible to 
    style the label in the desired way as with gtk_label_set_xalign(). The warning is surpressed by the use of following macros.
*/
#if GLIB_CHECK_VERSION(2,32,0)
G_GNUC_BEGIN_IGNORE_DEPRECATIONS
#endif
    gtk_misc_set_alignment (GTK_MISC (ks.fr_replacements_done_label), 0.0, 0.5);
#if GLIB_CHECK_VERSION(2,32,0)
G_GNUC_END_IGNORE_DEPRECATIONS
#endif
#endif

    GtkWidget *content_area = gtk_dialog_get_content_area (GTK_DIALOG (ks.fr_dialog));
    GtkWidget *grid = gtk_grid_new ();
    GtkWidget *columns_grid = gtk_grid_new ();
    GtkWidget *special_options_grid = gtk_grid_new ();
    enum { FR_REPLACE_LABEL, FR_IN_LABEL, FR_WITH_LABEL, NUMBER_OF_FR_LABEL_FIELDS };
    gchar *fr_label_txts[] = { "<b>Replace: </b>", "<b>In:</b>", "<b>With:</b>" };
    GtkWidget *fr_labels[NUMBER_OF_FR_LABEL_FIELDS] = { gtk_label_new (NULL), gtk_label_new (NULL), gtk_label_new (NULL) };
    gchar *special_options_txts[] = { "Match case", "Regular expression (PCRE)", "Whole word" };

    gint8 buttons_cnt, columns_cnt, labels_cnt;

    for (labels_cnt = 0; labels_cnt < NUMBER_OF_FR_LABEL_FIELDS; labels_cnt++) {
#if GTK_CHECK_VERSION(3,16,0)
        gtk_label_set_xalign (GTK_LABEL (fr_labels[labels_cnt]), 0.0);
#else
/* 
    This would result in a warning when compiling with GTK 3.14, because gtk_misc_set_alignment() has been deprecated with 3.14, 
    but gtk_label_set_xalign is only available since 3.16. With gtk_widget_set_halign() it is not possible to 
    style the label in the desired way as with gtk_label_set_xalign(). The warning is surpressed by the use of following macros.
*/
#if GLIB_CHECK_VERSION(2,32,0)
G_GNUC_BEGIN_IGNORE_DEPRECATIONS
#endif
        gtk_misc_set_alignment (GTK_MISC (fr_labels[labels_cnt]), 0.0, 0.5);
#if GLIB_CHECK_VERSION(2,32,0)
G_GNUC_END_IGNORE_DEPRECATIONS
#endif
#endif
        gtk_label_set_markup (GTK_LABEL (fr_labels[labels_cnt]), fr_label_txts[labels_cnt]);
    }

    gtk_grid_attach (GTK_GRID (grid), fr_labels[FR_REPLACE_LABEL], 0, 0, 1, 1);
    gtk_grid_attach (GTK_GRID (grid), ks.fr_entry_fields[REPLACE_ENTRY], 1, 0, 1, 1);
    gtk_widget_set_hexpand (ks.fr_entry_fields[REPLACE_ENTRY], TRUE);

    gtk_grid_attach (GTK_GRID (grid), fr_labels[FR_IN_LABEL], 0, 1, 1, 1);
    gtk_grid_attach (GTK_GRID (grid), columns_grid, 1, 1, 1, 1);

    for (columns_cnt = 0; columns_cnt < COL_ELEMENT_VISIBILITY; columns_cnt++) {
        if (columns_cnt == COL_TYPE) { // The text for the type of a menu element has to be unmodifiable.
            continue;
        }

        ks.fr_in_columns[columns_cnt] = gtk_check_button_new_with_label (ks.column_header_txts[columns_cnt]);
        gtk_container_add (GTK_CONTAINER (columns_grid), ks.fr_in_columns[columns_cnt]);

        ks.fr_in_columns_css_providers[columns_cnt] = gtk_css_provider_new ();
        gtk_style_context_add_provider (gtk_widget_get_style_context (ks.fr_in_columns[columns_cnt]), 
                                        GTK_STYLE_PROVIDER (ks.fr_in_columns_css_providers[columns_cnt]), 
                                        GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
    }

    ks.fr_in_all_columns = gtk_check_button_new_with_label ("All modifiable columns");
    gtk_container_add (GTK_CONTAINER (columns_grid), ks.fr_in_all_columns);
    ks.fr_in_all_columns_css_provider = gtk_css_provider_new ();
    gtk_style_context_add_provider (gtk_widget_get_style_context (ks.fr_in_all_columns), 
                                    GTK_STYLE_PROVIDER (ks.fr_in_all_columns_css_provider), 
                                    GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

    gtk_grid_attach (GTK_GRID (grid), special_options_grid, 1, 2, 1, 1);
    for (buttons_cnt = 0; buttons_cnt < NUMBER_OF_SPECIAL_OPTIONS; buttons_cnt++) {
        ks.fr_special_options[buttons_cnt] = gtk_check_button_new_with_label (special_options_txts[buttons_cnt]);
        gtk_container_add (GTK_CONTAINER (special_options_grid), ks.fr_special_options[buttons_cnt]);
    }

    gtk_grid_attach (GTK_GRID (grid), ks.fr_replacements_done_label, 1, 3, 1, 1);
    gtk_widget_set_margin_top (ks.fr_replacements_done_label, 3);
    gtk_widget_set_margin_bottom (ks.fr_replacements_done_label, 3);

    gtk_grid_attach (GTK_GRID (grid), fr_labels[FR_WITH_LABEL], 0, 4, 1, 1);
    gtk_grid_attach (GTK_GRID (grid), ks.fr_entry_fields[WITH_ENTRY], 1, 4, 1, 1);
    gtk_widget_set_hexpand (ks.fr_entry_fields[WITH_ENTRY], TRUE);

    gtk_container_add (GTK_CONTAINER (content_area), grid);
    gtk_container_set_border_width (GTK_CONTAINER (content_area), 5);

    g_signal_connect_swapped (ks.fr_entry_fields[REPLACE_ENTRY], "changed", G_CALLBACK (entry_field_content_changed), NULL);
    g_signal_connect_swapped (ks.fr_entry_fields[WITH_ENTRY], "changed", G_CALLBACK (entry_field_content_changed), NULL);
    for (columns_cnt = 0; columns_cnt < COL_ELEMENT_VISIBILITY; columns_cnt++) {
        if (columns_cnt != COL_TYPE) {
            ks.handler_id_fr_in_columns[columns_cnt] = g_signal_connect_swapped (ks.fr_in_columns[columns_cnt], "clicked", 
                                                                                 G_CALLBACK (fr_buttons_management), "specif");
        }
    }
    g_signal_connect_swapped (ks.fr_in_all_columns, "clicked", G_CALLBACK (fr_buttons_management), "all");
    g_signal_connect_swapped (gtk_dialog_get_widget_for_response (GTK_DIALOG (ks.fr_dialog), 1), "clicked", 
                              G_CALLBACK (find_and_replace_intialisation), NULL);
    g_signal_connect_swapped (gtk_dialog_get_widget_for_response (GTK_DIALOG (ks.fr_dialog), 2), "clicked", 
                              G_CALLBACK (gtk_widget_destroy), ks.fr_dialog);

    // Default settings
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (ks.fr_in_all_columns), TRUE);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (ks.fr_special_options[REGULAR_EXPRESSION]), TRUE);
    gtk_dialog_set_response_sensitive (GTK_DIALOG (ks.fr_dialog), 1, FALSE);

    gtk_widget_show_all (ks.fr_dialog);

    gtk_widget_set_size_request (ks.fr_dialog, 570, -1);
    gtk_window_set_position (GTK_WINDOW (ks.fr_dialog), GTK_WIN_POS_CENTER_ALWAYS);
}
