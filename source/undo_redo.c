/*
   Kickshaw - A Menu Editor for Openbox

   Copyright (c) 2010–2019        Marcus Schätzle

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along 
   with Kickshaw. If not, see http://www.gnu.org/licenses/.
*/

#include <gtk/gtk.h>
#include <stdlib.h>
#include <string.h>

#include "definitions_and_enumerations.h"
#include "undo_redo.h"

static gboolean write_row_information (GtkTreeModel  *model, 
                                       GtkTreePath   *path, 
                                       GtkTreeIter   *iter, 
                                       GOutputStream *compressed_undo_file_output_stream);
void push_new_item_on_undo_stack (void);
void undo_redo (gchar *action);

/* 

    Writes everything relevant needed to restore the data and appearance of a single tree view row, including:
    - Tree store fields (of the image related fields, only the file path is used, 
      since there might have occurred changes to the image file in the time between the storage of the data 
      and its retrieval).
    - Existence of a label
    - Path depth
    - Selection status
    - Expansion status

*/

static gboolean write_row_information (GtkTreeModel  *model,
                                       GtkTreePath   *path, 
                                       GtkTreeIter   *iter, 
                                       GOutputStream *compressed_undo_file_output_stream)
{
    GtkTreeSelection *selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (ks.treeview));
    GString *row_info = g_string_new (NULL);
    GError *error = NULL;

    gchar *undo_txt_fields[NUMBER_OF_TXT_FIELDS];

    guint8 tree_store_loop;

    for (tree_store_loop = 0; tree_store_loop < NUMBER_OF_TXT_FIELDS; tree_store_loop++) {
        /*
            Reading of the tree store data has to start from TS_ICON_PATH,
            since TS_ICON_IMG, TS_ICON_IMG_STATUS and TS_ICON_MODIFICATION_TIME aren't used.
        */
        gtk_tree_model_get (model, iter, tree_store_loop + TS_ICON_PATH, &undo_txt_fields[tree_store_loop], -1);
        g_string_append_printf (row_info, "%s%c", 
                                (undo_txt_fields[tree_store_loop]) ? undo_txt_fields[tree_store_loop] : "\0", 
                                31); // 31 = unit separator
    }

    g_string_append_printf (row_info, "%s%c%i%c%s%c%s%c", 
                            (streq_any (undo_txt_fields[TYPE_TXT], "menu", "pipe menu", "item", "separator", NULL) && 
                             undo_txt_fields[MENU_ELEMENT_TXT]) ? "TRUE" : "FALSE", // Existence of a label
                            31, // 31 = unit separator
                            gtk_tree_path_get_depth (path), // Path depth
                            31, // 31 = unit separator
                            (gtk_tree_selection_iter_is_selected (selection, iter)) ? "TRUE" : "FALSE", // Selection status
                            31, // 31 = unit separator
                            // Expansion status
                            (gtk_tree_view_row_expanded (GTK_TREE_VIEW (ks.treeview), path)) ? "TRUE" : "FALSE",
                            30); // 30 = record/row separator

    if (G_UNLIKELY (!(g_output_stream_write (compressed_undo_file_output_stream, 
                                             row_info->str, strlen (row_info->str), NULL, &error)))) {
        gchar *errmsg_txt = g_strdup_printf ("<b>An error occurred during the writing process of the new undo stack item</b> "
                                             "<tt>%i</tt> <b>inside</b> <tt>%s</tt> <b>!\nError:</b> %s", 
                                             ks.pos_inside_undo_stack, ks.tmp_path, error->message);

        show_errmsg (errmsg_txt);

        // Restore position index to the status prior to the creation of the new item.
        ks.pos_inside_undo_stack--;

        // Cleanup
        g_free (errmsg_txt);
    }

    // Cleanup
    free_elements_of_static_string_array (undo_txt_fields, NUMBER_OF_TXT_FIELDS, FALSE);
    g_string_free (row_info, TRUE);

    if (G_LIKELY (!error)) {
        return FALSE; // FALSE = continue iterating.
    }
    else {
        // Cleanup
        g_error_free (error);

        return TRUE; // TRUE = stop iterating.
    }
}

/* 

    Pushes tree view data on the undo stack, 
    i.e. writes this data into a compressed file insde the tmp folder.

*/

void push_new_item_on_undo_stack (void)
{
    GError *error = NULL;

    /*
        Applying a change starting from a position earlier than the latest undo file creates a new "time line".
        This means that all later undo files starting from the current position have to be removed.
    */
    if (ks.pos_inside_undo_stack < ks.number_of_undo_stack_items) {
        gint stack_file_cnt;

        for (stack_file_cnt = ks.pos_inside_undo_stack + 1; stack_file_cnt <= ks.number_of_undo_stack_items; stack_file_cnt++) {
            gchar *later_undo_file_str = g_strdup_printf ("%s/%i", ks.tmp_path, stack_file_cnt);
            GFile *later_undo_file = g_file_new_for_path (later_undo_file_str);

            if (G_UNLIKELY (!(g_file_delete (later_undo_file, NULL, &error)))) {
                gchar *unsaved_new_undo_stack_item = g_strdup_printf ("%i", ks.pos_inside_undo_stack + 1);
                gchar *err_msg = g_strdup_printf ("<b>Could not remove undo stack item</b> <tt>%i</tt> "
                                                  "<b>inside tmp folder</b> <tt>%s</tt> <b>!\nError:</b> %s\n\n"
                                                  "--- Note ---\n\n"
                                                  "All undo stack items after the current position are supposed "
                                                  "to be deleted when a change is applied prior to the latest change, "
                                                  "i.e. when it is applied from a position within the undo stack.\n"
                                                  "A new undo stack item '<tt>%s</tt>' for this change won't be saved "
                                                  "so it will not be possible to recall this change by invoking an "
                                                  "undo or redo action.", 
                                                  stack_file_cnt, ks.tmp_path, error->message, unsaved_new_undo_stack_item);

                show_errmsg (err_msg);

                // Cleanup
                g_free (err_msg);
            }

            // Cleanup
            g_free (later_undo_file_str);
            g_object_unref (later_undo_file);

            if (G_UNLIKELY (error)) {
                // Cleanup
                g_error_free (error);

                return;
            }
        }

        ks.number_of_undo_stack_items = ks.pos_inside_undo_stack;
    }

    ks.pos_inside_undo_stack = ks.number_of_undo_stack_items + 1;
    gchar *compressed_undo_file_str = g_strdup_printf ("%s/%i", ks.tmp_path, ks.pos_inside_undo_stack);
    GFile *compressed_undo_file = g_file_new_for_path (compressed_undo_file_str);
    GOutputStream *undo_file_output_stream;

    if (G_UNLIKELY (!((undo_file_output_stream = G_OUTPUT_STREAM (g_file_create (compressed_undo_file, G_FILE_CREATE_NONE, 
                                                                                 FALSE, &error)))))) {
        gchar *err_msg = g_strdup_printf ("<b>Could not create undo stack item</b> <tt>%i</tt> "
                                          "<b>inside tmp folder</b> <tt>%s</tt> <b>!\nError:</b> %s", 
                                          ks.pos_inside_undo_stack, ks.tmp_path, error->message);

        show_errmsg (err_msg);

        // Restore position index to the status prior to the creation of the new item.
        ks.pos_inside_undo_stack--;

        // Cleanup
        g_error_free (error);
        g_free (err_msg);
        g_free (compressed_undo_file_str);
        g_object_unref (compressed_undo_file);

        return;
    }

    GConverter *compressor = G_CONVERTER (g_zlib_compressor_new (G_ZLIB_COMPRESSOR_FORMAT_RAW, -1));
    GOutputStream *compressed_undo_file_output_stream = g_converter_output_stream_new (undo_file_output_stream, compressor);

    gtk_tree_model_foreach (ks.model, (GtkTreeModelForeachFunc) write_row_information, compressed_undo_file_output_stream);

    // The position index has been reset by the callback function write_row_information (), meaning that there has been an error.
    if (ks.pos_inside_undo_stack == ks.number_of_undo_stack_items && 
        G_UNLIKELY (!(g_file_delete (compressed_undo_file, NULL, &error)))) {
        gchar *err_msg = g_strdup_printf ("<b>Could not remove incomplete undo stack item</b> <tt>%i</tt> "
                                          "<b>inside tmp folder</b> <tt>%s</tt> <b>!\n" 
                                          "Error:</b> %s", ks.pos_inside_undo_stack, ks.tmp_path, error->message);

        show_errmsg (err_msg);

        // Cleanup
        g_error_free (error);
        g_free (err_msg);
    }

    // Cleanup
    g_free (compressed_undo_file_str);
    g_object_unref (compressed_undo_file);
    g_object_unref (undo_file_output_stream);
    g_object_unref (compressor);
    g_object_unref (compressed_undo_file_output_stream);

    // There has been an error, do not increase ks.number_of_undo_stack_items.
    if (ks.pos_inside_undo_stack == ks.number_of_undo_stack_items) {
        return;
    }

    ks.number_of_undo_stack_items = ks.pos_inside_undo_stack;
}

/* 

    Undos the last or redos the next change from the current position inside the undo stack.

*/

void undo_redo (gchar *action)
{
    gint new_pos_inside_undo_stack = (STREQ (action, "undo")) ? ks.pos_inside_undo_stack - 1 : ks.pos_inside_undo_stack + 1;
    gchar *compressed_undo_file_str = g_strdup_printf ("%s/%i", ks.tmp_path, new_pos_inside_undo_stack);
    GFile *compressed_undo_file = g_file_new_for_path (compressed_undo_file_str);
    GFileInputStream *undo_file_input_stream;
    GError *error = NULL;

    if (G_UNLIKELY (!((undo_file_input_stream = g_file_read (compressed_undo_file, NULL, &error))))) {
        gchar *errmsg_txt = g_strdup_printf ("<b>Could not open undo stack item</b> <tt>%i</tt> "
                                             "<b>inside tmp folder</b> <tt>%s</tt> <b>for reading !\n"
                                             "Error:</b> %s", new_pos_inside_undo_stack, ks.tmp_path, error->message);

        show_errmsg (errmsg_txt);

        // Cleanup
        g_error_free (error);
        g_free (errmsg_txt);
        g_free (compressed_undo_file_str);
        g_object_unref (compressed_undo_file);

        return;
    }

    GtkTreeSelection *selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (ks.treeview));

    g_signal_handler_block (selection, ks.handler_id_row_selected);


    // --- Clear global data. ---


    g_slist_free_full (ks.menu_ids, (GDestroyNotify) g_free);
    ks.menu_ids = NULL;

    if (ks.rows_with_icons) {
        stop_timeout ();
    }

    gtk_tree_store_clear (ks.treestore);

    ks.statusbar_msg_shown = FALSE;


    // --- Restore tree store from undo file. ---


    GConverter *decompressor = G_CONVERTER (g_zlib_decompressor_new (G_ZLIB_COMPRESSOR_FORMAT_RAW));
    GInputStream *decompressed_undo_file_input_stream = g_converter_input_stream_new (G_INPUT_STREAM (undo_file_input_stream), decompressor);
    GString *record = g_string_new (NULL);
    gchar buffer[1];

    gchar **tokens;

    // The fields that are stored inside an undo file.
    enum { UN_ICON_PATH, UN_MENU_ELEMENT, UN_TYPE, UN_VALUE, UN_MENU_ID, UN_EXECUTE, UN_ELEMENT_VISIBILITY, 
           UN_MENU_ITEM_OR_SEP_WITH_LABEL, UN_PATH_DEPTH, UN_SELECTED, UN_EXPANDED };

    GtkTreeIter *iters = g_malloc (sizeof (GtkTreeIter)); 
    GtkTreePath *path;

    GSList *expanded_paths = NULL;

    guint path_depth;
    guint max_path_depth = 1; // Default
    guint iter_idx;

    guint8 tokens_cnt;
    GSList *expanded_paths_loop;

    while (g_input_stream_read (decompressed_undo_file_input_stream, buffer, 1, NULL, &error) > 0) {
        if (G_LIKELY (!(STREQ (buffer, "\x1E")))) {
            g_string_append (record, buffer);
        }
        else { // x1E (dec. 30) = record/row separator
            tokens = g_strsplit (record->str, "\x1F", -1); // x1F (dec. 31) = unit separator

            path_depth = atoi (tokens[UN_PATH_DEPTH]);
            if (path_depth > max_path_depth) {
                max_path_depth = path_depth;
                iters = g_realloc (iters, sizeof (GtkTreeIter) * max_path_depth);
            }

            iter_idx = path_depth - 1;
            gtk_tree_store_insert (ks.treestore, &iters[iter_idx], (path_depth == 1) ? NULL : &iters[iter_idx - 1], -1);

            for (tokens_cnt = 0; tokens_cnt <= UN_ELEMENT_VISIBILITY; tokens_cnt++) {
                gtk_tree_store_set (ks.treestore, &iters[iter_idx], tokens_cnt + TS_ICON_PATH, 
                                    (!STREQ (tokens[tokens_cnt], "\0")) ? tokens[tokens_cnt] : 
                                     /*
                                        Menus, items and separators may have empty labels or no labels at all.
                                        Menu IDs may consist of empty strings.
                                     */
                                     (((tokens_cnt == UN_MENU_ELEMENT && STREQ (tokens[UN_MENU_ITEM_OR_SEP_WITH_LABEL], "TRUE")) ||
                                      tokens_cnt == UN_MENU_ID) ? 
                                      "\0" : NULL), 
                                      -1);
            }

            if (streq_any (tokens[UN_TYPE], "menu", "pipe menu", NULL)) {
                ks.menu_ids = g_slist_prepend (ks.menu_ids, g_strdup (tokens[UN_MENU_ID]));
            }

            if (*tokens[UN_ICON_PATH]) {
                GFile *icon = g_file_new_for_path (tokens[UN_ICON_PATH]);

                if (G_LIKELY (g_file_query_exists (icon, NULL))) {
                    if (G_UNLIKELY (!(set_icon (&iters[iter_idx], tokens[UN_ICON_PATH], FALSE, TRUE)))) {
                        gchar *time_stamp = get_modification_time_for_icon (tokens[UN_ICON_PATH]);

                        gtk_tree_store_set (ks.treestore, &iters[iter_idx],
                                            TS_ICON_IMG, ks.invalid_icon_imgs[INVALID_FILE_ICON], 
                                            TS_ICON_IMG_STATUS, INVALID_FILE, 
                                            TS_ICON_MODIFICATION_TIME, time_stamp, 
                                            -1);

                        // Cleanup
                        g_free (time_stamp);
                    }
                }
                else {
                    gtk_tree_store_set (ks.treestore, &iters[iter_idx],
                                        TS_ICON_IMG, ks.invalid_icon_imgs[INVALID_PATH_ICON], 
                                        TS_ICON_IMG_STATUS, INVALID_PATH, 
                                        TS_ICON_MODIFICATION_TIME, NULL, 
                                        -1);
                }

                // Cleanup
                g_object_unref (icon);
            }

            path = gtk_tree_model_get_path (ks.model, &iters[iter_idx]);
            if (STREQ (tokens[UN_SELECTED], "TRUE")) {
                gtk_tree_selection_select_path (selection, path);
            }

            if (STREQ (tokens[UN_EXPANDED], "TRUE")) {
                expanded_paths = g_slist_prepend (expanded_paths, gtk_tree_path_copy (path));
            }

            g_string_assign (record, "");

            // Cleanup
            g_strfreev (tokens);
            gtk_tree_path_free (path);
        }
    }

    if (G_LIKELY (!error)) {
        expanded_paths = g_slist_reverse (expanded_paths);
        for (expanded_paths_loop = expanded_paths; expanded_paths_loop; expanded_paths_loop = expanded_paths_loop->next) {
            gtk_tree_view_expand_row (GTK_TREE_VIEW (ks.treeview), expanded_paths_loop->data, FALSE);
        }
    }

    g_signal_handler_unblock (selection, ks.handler_id_row_selected);

    // Cleanup
    g_free (compressed_undo_file_str);
    g_object_unref (compressed_undo_file);
    g_object_unref (undo_file_input_stream);
    g_object_unref (decompressor);
    g_object_unref (decompressed_undo_file_input_stream);
    g_string_free (record, TRUE);
    g_free (iters);
    g_slist_free_full (expanded_paths, (GDestroyNotify) gtk_tree_path_free);

    if (G_UNLIKELY (error)) {
        gchar *errmsg_txt = g_strdup_printf ("<b>Could not read data from undo stack item</b> <tt>%i</tt> "
                                             "<b>inside tmp folder</b> <tt>%s</tt> <b>!\n"
                                             "Error:</b> %s", new_pos_inside_undo_stack, ks.tmp_path, error->message);

        show_errmsg (errmsg_txt);

        // Cleanup
        g_error_free (error);
        g_free (errmsg_txt);

        return;
    }

    ks.pos_inside_undo_stack = new_pos_inside_undo_stack;
    
    activate_change_done ();  
    row_selected ();
}
